﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using NDesk.Options;
using System.Reflection;
using System.IO;

namespace IsProgramInstalled
{
    class Program
    {
        static int Main(string[] args)
        {
            bool show_all = false;
            bool show_help = false;
            bool match_exact = false;
            bool match_fuzzy = false;
            bool mode_quiet = false;
            bool mode_bare = false;
            bool mode_x86 = false;
            bool mode_x64 = false;

            string entry_name = "";


            var optionSet = new OptionSet()
            {
                { "s|showall" , "Prints all installed program entries from registry to stdout", v => show_all = v != null },
                { "e|exact", "Return true for exact string match for entry in registry",  v => match_exact = v !=null },
                { "n|name=", "String to test against", v => entry_name = v },
                { "f|fuzzy", "Fuzzy match (sub-string search) for entry. CAUTION.", v => match_fuzzy = v !=null },
                { "8|x86", "Get only entries from the x86 (32bit) program registry (on 64bit machines).", v => mode_x86=v!=null },
                { "6|x64", "Get only entries from the x86-64 (64bit) program registry (on 64bit machines).", v => mode_x64=v!=null },
                { "b|bare", "Only print matched registry entry name string", v => mode_bare = v!=null },
                { "q|quiet", "No output to stdout, only return value", v => mode_quiet = v !=null },
                { "h|?|help", "Show help", v => show_help = v !=null}
            };

            List<string> extra_opts = new List<string>();
            try
            {
                extra_opts = optionSet.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Error.Write("Fatal error parsing arguments: " + string.Join(" ", args));
                Console.Error.WriteLine(e.Message);
            }

            if (!(mode_x64 ^ mode_x86))
            {
                // neither or both set
                // make both on
                mode_x64 = true;
                mode_x86 = true;
            }

            if (mode_bare && mode_quiet)
            {
                Console.Error.WriteLine("Both bare and quiet modes selected. Conflicting arguments, please choose only one.");
                show_help = true;
            }

            if (match_exact && match_fuzzy)
            {
                Console.Error.WriteLine("Both exact match and fuzzy match selected. Conflicting arguments, please choose only one.");
                show_help = true;
            }

            if (match_exact || match_fuzzy)
            {
                if (string.IsNullOrEmpty(entry_name) || string.IsNullOrWhiteSpace(entry_name))
                {
                    Console.Error.WriteLine("Name to search for is unset or empty. Please specify.");
                    show_help = true;
                }
            }

            // we got here through no valid options.
            if (extra_opts.Count > 0)
            {
                Console.Error.WriteLine("Unknown option(s): " + string.Join(", ", extra_opts.ToArray()));
                show_help = true;
            }

            if (show_help)
            {
                ShowHelp(optionSet);
                return -1;
            }

            if (show_all)
            {
                ShowAll(mode_x86, mode_x64);
                return 0;
            }


            if (match_exact) return ExactMatchTest(entry_name, mode_quiet, mode_bare, mode_x86, mode_x64);

            if (match_fuzzy) return FuzzyMatchTest(entry_name, mode_quiet, mode_bare, mode_x86, mode_x64);

            // we should never get here unless something else goes wrong. show help

            ShowHelp(optionSet);

            return -1;
        }

        // HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall

        private static int FuzzyMatchTest(string entry_name, bool mode_quiet, bool mode_bare, bool mode_x86, bool mode_x64)
        {
            bool error_x64 = false;
            bool error_x86 = false;
            Exception ex86 = null;
            Exception ex64 = null;

            if (mode_x64)
            {
                try
                {
                    // x64
                    string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
                    using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                    {
                        foreach (string subkey_name in key.GetSubKeyNames())
                        {
                            using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                            {
                                object subkeyValue = subkey.GetValue("DisplayName");
                                if (subkeyValue != null)
                                {
                                    string strHumanReadableName = subkey.GetValue("DisplayName").ToString();
                                    if (strHumanReadableName.ToLower().Contains(entry_name.Trim().ToLower()))
                                    {
                                        if (!mode_quiet)
                                        {
                                            if (!mode_bare)
                                                Console.Write("Fuzzy match found: ");
                                            Console.WriteLine(strHumanReadableName);
                                        }
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    error_x64 = true;
                    ex64 = e;
                }
            }

            if (mode_x86)
            {
                try
                {
                    // x86
                    string registry_key = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
                    using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                    {
                        foreach (string subkey_name in key.GetSubKeyNames())
                        {
                            using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                            {
                                object subkeyValue = subkey.GetValue("DisplayName");
                                if (subkeyValue != null)
                                {
                                    string strHumanReadableName = subkey.GetValue("DisplayName").ToString();
                                    if (strHumanReadableName.ToLower().Contains(entry_name.Trim().ToLower()))
                                    {
                                        if (!mode_quiet)
                                        {
                                            if (!mode_bare)
                                                Console.Write("Fuzzy match found: ");
                                            Console.WriteLine(strHumanReadableName);
                                        }
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e2)
                {
                    error_x86 = true;
                    ex86 = e2;
                }
            }

            if (error_x64)
            {
                if (!mode_quiet)
                    Console.Error.WriteLine("Unable to open registry for x64: " + ex64.Message);
            }
            if (error_x86)
            {
                if (!mode_quiet)
                    Console.Error.WriteLine("Unable to open reigstry for x86 (WOW64): " + ex86.Message);
            }
            if (error_x86 || error_x64)
                return -1;

            if (!mode_quiet) Console.WriteLine("No match found: " + entry_name);
            return 0;
        }

        private static int ExactMatchTest(string entry_name, bool mode_quiet, bool mode_bare, bool mode_x86, bool mode_x64)
        {

            //x64
            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
            {
                foreach (string subkey_name in key.GetSubKeyNames())
                {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                    {
                        object subkeyValue = subkey.GetValue("DisplayName");
                        if (subkeyValue != null)
                        {
                            string strHumanReadableName = subkey.GetValue("DisplayName").ToString();
                            if (strHumanReadableName.Trim().ToLower().Equals(entry_name.Trim().ToLower()))
                            {
                                if (!mode_quiet)
                                {
                                    if (!mode_bare)
                                        Console.Write("Exact match found: ");
                                    Console.WriteLine(strHumanReadableName);
                                }
                                return 1;
                            }
                        }
                    }
                }
            }

            // x86
            registry_key = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
            using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
            {
                foreach (string subkey_name in key.GetSubKeyNames())
                {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                    {
                        object subkeyValue = subkey.GetValue("DisplayName");
                        if (subkeyValue != null)
                        {
                            string strHumanReadableName = subkey.GetValue("DisplayName").ToString();
                            if (strHumanReadableName.Trim().ToLower().Equals(entry_name.Trim().ToLower()))
                            {
                                if (!mode_quiet)
                                {
                                    if (!mode_bare)
                                        Console.Write("Exact match found: ");
                                    Console.WriteLine(strHumanReadableName);
                                }
                                return 1;
                            }
                        }
                    }
                }
            }

            if (!mode_quiet) Console.WriteLine("No match found: " + entry_name);
            return 0;
        }

        private static void ShowAll(bool mode_x86, bool mode_x64)
        {
            int count = 0;

            // problem with x86/64 here, show all, x64 first

            if (mode_x64)
            {
                Console.WriteLine("x64 Entries:");

                string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    foreach (string subkey_name in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                        {
                            object subkeyValue = subkey.GetValue("DisplayName");
                            if (subkeyValue != null)
                            {
                                string strHumanReadableName = subkey.GetValue("DisplayName").ToString();
                                Console.WriteLine(strHumanReadableName);
                            }
                        }
                    }
                    count += key.GetSubKeyNames().Length;
                }
                Console.WriteLine();
            }

            if (mode_x86)
            {
                Console.WriteLine("x86 Entries:");

                string registry_key = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    foreach (string subkey_name in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                        {
                            object subkeyValue = subkey.GetValue("DisplayName");
                            if (subkeyValue != null)
                            {
                                string strHumanReadableName = subkey.GetValue("DisplayName").ToString();
                                Console.WriteLine(strHumanReadableName);
                            }
                        }
                    }
                    count += key.GetSubKeyNames().Length;
                }
            }

            Console.WriteLine("Number of items: " + count);
        }

        private static void ShowHelp(OptionSet optionSet)
        {
            Console.WriteLine(GetExecutableName() + " by Doug Barry @ UOG SOE 20131107");
            Console.WriteLine("Usage: " + GetExecutableName() + " [options]");
            Console.WriteLine("Query the windows registry for information on programs that are" +
                "\r\ninstalled. By default both x64 and x86 entries are used, which may contain" + 
                "\r\nduplicates. NOTE: All string searches are case-insensitive. When using" +
                "\r\nfuzzy match, the first match is returned, from the unsorted registry list" +
                "\r\nwith the 64bit registry first.");
            Console.WriteLine();
            Console.WriteLine("The program has a return value:");
            Console.WriteLine("\t=-1:\tError condition, information written to stderr.");
            Console.WriteLine("\t= 0:\tNo error, and no success. Program not found, or no operation" +
                "\r\n\t\t\tperformed.");
            Console.WriteLine("\t= 1:\tProgram found sucessfully.");
            Console.WriteLine();
            Console.WriteLine("Options:");
            optionSet.WriteOptionDescriptions(Console.Out);
            Console.WriteLine("Example:");
            Console.WriteLine("\tShow all 32bit entries:");
            Console.WriteLine("\t\tIsProgramInstalled -8 -s");
            Console.WriteLine();
            Console.WriteLine("\tFuzzy match on Adobe Air in 32 or 64 bit registry:");
            Console.WriteLine("\t\tIsProgramInstalled -f -name=\"Adobe Air\"");
            Console.WriteLine("\t\techo %ERRORLEVEL");
            Console.WriteLine("\t\t\tReturns \"1\"");
        }

        private static string GetExecutableName()
        {
            return Path.GetFileNameWithoutExtension(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
        }

    }
}